# apijson-boot-source

#### 介绍
个人研究只用

#### 软件架构

- 此项目是在apijson-boot 的基础上将orm,framework,unitautoapp,io.github.classgraph
,nonapi.io.github.classgraph.untiauto 等包以源码形式加入到了项目，目的是
查看内容的时候方便查看，或者自己修改
- 此项目基本上保持了apijson-boot的基本结构没有变化

#### 安装教程
- 下载之后修改DemoSQLConfig.java中的连接数据库的信息，目前之类默认启动mysql
,修改之后直接运行DemoApplication.java就可以了

#### 使用说明
- 相关调用可以使用apijson-auto 来进行调用

